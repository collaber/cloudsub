#!/usr/bin/env python

"""Copyright 2010 Phidgets Inc.
This work is licensed under the Creative Commons Attribution 2.5 Canada License. 
To view a copy of this license, visit http://creativecommons.org/licenses/by/2.5/ca/
"""

__author__ = 'Adam Stelmack'
__version__ = '2.1.8'
__date__ = 'May 17 2010'

#Basic imports
from ctypes import *
import sys
import random
import time
#Phidget specific imports
from Phidgets.PhidgetException import PhidgetErrorCodes, PhidgetException
from Phidgets.Events.Events import AttachEventArgs, DetachEventArgs, ErrorEventArgs, InputChangeEventArgs, OutputChangeEventArgs, SensorChangeEventArgs
from Phidgets.Devices.InterfaceKit import InterfaceKit
from Phidgets.Phidget import PhidgetLogLevel

# vendors_PH_formula:    ph = e.value * 0.0178 - 1.889
# calibrated formula:   ph = e.value * 0.0196 - 2.2574
## t=20C
## PH4 values: 320, 323, 321, 322, 318, 315. AVG = 319.83
## PH7 values: 472, 474, 473, 474, 478, 468. AVG = 473.17
# 
# for pool water transient state duration was around 7 min. [ 00:59:28 - 01:06:33 ] (significance of PH error < 0.05)
# duration can be established as: time it takes for abs value to change for 3+ in Tn < 2*T[n-1]
# for PH buffers transient state duration is less than 10 sec.

def usage():
    print("Usage: %s [--ph (0|1)] [--orp (1|0)]")
    print("ID of the port resepctive sensor is connected to: 0 or 1")
    print("IDs can't be the same, e.g. --ph=1 --orp=1")
    print("If sensor is omitted, respective port is ignored")


# read params
ph_sensor_id = -1
orp_sensor_id = -1
if len(sys.argv) > 1:
    print(str(sys.argv))
    try:
        if sys.argv[1] == '--ph':
            ph_sensor_id = ord(sys.argv[2])
        elif sys.argv[1] == '--orp':
            orp_sensor_id = ord(sys.argv[2])
        else:
            usage()
            return 1
        if len(sys.argv) > 3:
            if sys.argv[3] == '--ph':
                ph_sensor_id = ord(sys.argv[4])
            elif sys.argv[3] == '--orp':
                orp_sensor_id = ord(sys.argv[4])
            else:
                usage()
                return 1
        if ph_sensor_id != -1 and orp_sensor_id != -1 :
            if orp_sensor_id == ph_sensor_id:
                usage()
                return 1
        if ph_sensor_id not in [-1, 0, 1] or orp_sensor_id not in [-1, 0, 1]:
            usage()
            return 1
    except:
        usage()
        return 1
else:
    print("One of sensors should be defined!")
    usage()
    return 1

#Create an interfacekit object
try:
    interfaceKit = InterfaceKit()
except RuntimeError as e:
    print("Runtime Exception: %s" % e.details)
    print("Exiting....")
    exit(1)

#Information Display Function
def displayDeviceInfo():
    print("|------------|----------------------------------|--------------|------------|")
    print("|- Attached -|-              Type              -|- Serial No. -|-  Version -|")
    print("|------------|----------------------------------|--------------|------------|")
    print("|- %8s -|- %30s -|- %10d -|- %8d -|" % (interfaceKit.isAttached(), interfaceKit.getDeviceName(), interfaceKit.getSerialNum(), interfaceKit.getDeviceVersion()))
    print("|------------|----------------------------------|--------------|------------|")
    print("Number of Digital Inputs: %i" % (interfaceKit.getInputCount()))
    print("Number of Digital Outputs: %i" % (interfaceKit.getOutputCount()))
    print("Number of Sensor Inputs: %i" % (interfaceKit.getSensorCount()))

#Event Handler Callback Functions
def interfaceKitAttached(e):
    attached = e.device
    print("InterfaceKit %i Attached!" % (attached.getSerialNum()))

def interfaceKitDetached(e):
    detached = e.device
    print("InterfaceKit %i Detached!" % (detached.getSerialNum()))

def interfaceKitError(e):
    try:
        source = e.device
        print("InterfaceKit %i: Phidget Error %i: %s" % (source.getSerialNum(), e.eCode, e.description))
    except PhidgetException as e:
        print("Phidget Exception %i: %s" % (e.code, e.details))

def interfaceKitSensorChanged(e):
    if e.index == 0:
        source = e.device
        ph = e.value * 0.0196 - 2.2574
        print("PH (sensor %i): %.3f / abs %i @ %s" % (e.index, ph, e.value, time.strftime("%H:%M:%S")))
    else:
        # ignore NC Sensor #1
        pass

#Main Program Code
try:
	#logging example, uncomment to generate a log file
    #interfaceKit.enableLogging(PhidgetLogLevel.PHIDGET_LOG_VERBOSE, "phidgetlog.log")
	
    interfaceKit.setOnAttachHandler(interfaceKitAttached)
    interfaceKit.setOnDetachHandler(interfaceKitDetached)
    interfaceKit.setOnErrorhandler(interfaceKitError)
    interfaceKit.setOnSensorChangeHandler(interfaceKitSensorChanged)
except PhidgetException as e:
    print("Phidget Exception %i: %s" % (e.code, e.details))
    print("Exiting....")
    exit(1)

print("Opening phidget object....")

try:
    interfaceKit.openPhidget()
except PhidgetException as e:
    print("Phidget Exception %i: %s" % (e.code, e.details))
    print("Exiting....")
    exit(1)

print("Waiting for attach....")

try:
    interfaceKit.waitForAttach(10000)
except PhidgetException as e:
    print("Phidget Exception %i: %s" % (e.code, e.details))
    try:
        interfaceKit.closePhidget()
    except PhidgetException as e:
        print("Phidget Exception %i: %s" % (e.code, e.details))
        print("Exiting....")
        exit(1)
    print("Exiting....")
    exit(1)
else:
    displayDeviceInfo()

print("Setting the data rate for each sensor index to 512ms....")
for i in range(interfaceKit.getSensorCount()):
    try:
        interfaceKit.setDataRate(i, 512)
    except PhidgetException as e:
        print("Phidget Exception %i: %s" % (e.code, e.details))

print("Setting the sensor sensitivity to %.3f / abs 3...." % (3 * 0.0196))
for i in range(interfaceKit.getSensorCount()):
    try:
        interfaceKit.setSensorChangeTrigger(i, 3)
    except PhidgetException as e:
        print("Phidget Exception %i: %s" % (e.code, e.details))

print("Press Enter to quit....")

chr = sys.stdin.read(1)

print("Closing...")

try:
    interfaceKit.closePhidget()
except PhidgetException as e:
    print("Phidget Exception %i: %s" % (e.code, e.details))
    print("Exiting....")
    exit(1)

print("Done.")
exit(0)
